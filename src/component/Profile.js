import React,{Component} from 'react';
import {Link} from "react-router-dom";
import {bindActionCreators} from "redux";
import connect from "react-redux/es/connect/connect";
import {setProfileDetails} from "../app/product/actions/profileActions";

class Profile extends Component{
  constructor(props){
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  componentDidMount() {
    this.props.setProfileDetails(this.props.match.params.id);
  }

  handleClick(){
    let userProfiled = this.props.match.params.id;
    let userName = this.props.profileReducer.profileDetails.name;
    let body = {"userProfiled":userProfiled,"userName":userName};

    fetch("http://localhost:8080/api/like",{
      method: 'post',
      headers: {
        'Content-Type':'application/json'
      },
      body: JSON.stringify(body)
    })
      .then(result => {
        console.log(`User Profile ${userProfiled} has been liked by ${userName}`)
      })
  }

  render(){
    const {profileDetails} = this.props.profileReducer;
    return (
      <div>
        <h3>User Profile</h3>
        <p>User name: {profileDetails.name}</p>
        <p>Gender: {profileDetails.gender}</p>
        <p>Desc: {profileDetails.description}</p>
        <button onClick={this.handleClick}>Like</button>
        <Link to='/'>Back to home</Link>
      </div>
    )
  }
}

const mapStateToPropsProfile = (state) => ({
  profileReducer: state.profileReducer
});
const mapDispatchToPropsProfile = (dispatch) => bindActionCreators({setProfileDetails},dispatch);


export default connect(mapStateToPropsProfile,mapDispatchToPropsProfile)(Profile);