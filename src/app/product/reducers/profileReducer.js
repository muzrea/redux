const initialState = {
  profileDetails: {}
};

export default (state = initialState,action) => {
  switch(action.type){
    case "SET_PROFILE_DETAILS":
      return {
        ...state,
        profileDetails: action.profileDetails
      };
    default:
      return state;
  }
}