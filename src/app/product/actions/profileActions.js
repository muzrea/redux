export const setProfileDetails = (id) => (dispatch) =>{
  fetch(`http://localhost:8080/api/user-profiles/${id}`)
    .then(res => res.json())
    .then(result => {
      dispatch({
        type: "SET_PROFILE_DETAILS",
        profileDetails: result
      })
    })
};